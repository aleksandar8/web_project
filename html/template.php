<?php


$username = 'root';
$password = 'root';
$database_type = 'mysql';
$database_host = 'localhost';
$database_name = 'web';

$id = $_GET['id'];


try
{
$connection = new PDO("$database_type:host=$database_host;dbname=$database_name", $username, $password);

$statement = $connection->prepare('SELECT * FROM web WHERE id = :id');

$statement->bindParam(':id', $id);

$statement->execute();

$contacts = $statement->fetch(PDO::FETCH_ASSOC);
	
}
catch(PDOException $e){
var_dump($e->getMessage());
}
?> 


<!DOCTYPE html>
<html>
<head>
	<title> <?php echo $contacts['title']; ?> </title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../css/style3.css">
</head>
<body>

<!-- Navigation -->
<div class="container-fluid">
	<nav class="navbar  navbar-fixed-top">
		<ul class="list-unstyled  list-inline pull-left ">
			<li><a href="#home">Дома</a></li>
			<li><a href="#about">За нас</a></li>
			<li><a href="#service"><?php echo $contacts['pORs']; ?></a></li>
			<li><a href="#contact">Контакт</a></li>
		</ul>
	</nav>
</div>


<!-- Title and subtitle -->
<div class="container-fluid cont" id="home">
	<div class="title text-center">
		<img src="<?php echo $contacts['cover'];  ?>">

		<div class="text centered">
			<h1> <?php echo $contacts['title']; ?>	</h1>
			<h3> <?php echo $contacts['subtitle'];  ?></h3>
		</div>
	</div>	
</div>

<!-- about & phone & location -->
<div class="container-fluidc about" id="about">
			<h2>За Нас</h2>
	<div class="row" >
		<div class="col-md-8 text-justify">
			<p>
				<?php echo $contacts['foryou']; ?>	
			</p>
		</div>
		<div class="col-md-4 text-center">
			<h4>Телефон:</h4>
			<p><?php echo $contacts['phone']; ?></p>
			<h4>Локација:</h4>
			<p><?php echo $contacts['location']; ?></p>
			
		</div>
	</div>
</div>

<!-- Service OR Product -->
<div class="container-fluid service" id="service">
	<h3 class="text-center"> <?php echo $contacts['pORs']; ?> </h3>
	<div class="row ">
		<div class="col-md-4 text-justify">
			<img src="<?php echo $contacts['url1']; ?>">
				
			<p>
				<?php echo $contacts['desc1']; ?>
				
			</p>
		</div>
			
		
		<div class="col-md-4 text-justify">
			<img src="<?php echo $contacts['url2']; ?>">
				
			<p>
				<?php echo $contacts['desc2']; ?>
				
			</p>
		</div>
			
		<div class="col-md-4 text-justify">
			<img src="<?php echo $contacts['url3']; ?>">
				
			<p>
				<?php echo $contacts['desc3']; ?>
				
			</p>
		</div>
	</div>
</div>

<!-- Contact -->
<div class="container-fluid contact" id="contact">
	<div class="row">
		<h3 class="text-center">Контакт</h3>
		<div class="col-md-7 forus">
			<p>
				<?php echo $contacts['about']; ?>		
		    </p>			
		</div>
		<div class="col-md-5">
			<form>
				<label>Име</label>
				<input class="form-control input-sm" placeholder="Вашето име" type="text" name="">

				<label>Емаил</label>
				<input  class="form-control input-sm" placeholder="Вашиот емаил" type="email" name="">

				<label>Порака</label>
				 <textarea class="form-control" placeholder="Вашата порака" rows="5" ></textarea>
				<button type="submit" class="btn btn-primary center-block"> Испрати</button>
			</form>
		</div>
	</div>
</div>

<!-- Footer -->
<footer >
	<div class="clearfix foot" >
		<div class="pull-left left"> 
		<p >Изработено од Александар Николовски &reg</p>
		</div>
		<div class="pull-right right">
			
		<ul class="list-unstyled list-inline  font">
			<li><a href="<?php echo $contacts['linkedin']; ?>"><i class="fa fa-linkedin-square fa-3x fa-inverse" aria-hidden="true"></i></a></li>
			<li><a href="<?php echo $contacts['facebook']; ?>"><i class="fa fa-facebook-square fa-3x fa-inverse" aria-hidden="true"></i></a></li>
			<li><a href="<?php echo $contacts['twitter']; ?>"><i class="fa fa-twitter-square fa-3x fa-inverse" aria-hidden="true"></i></a></li>
			<li><a href="<?php echo $contacts['google']; ?>"><i class="fa fa-google-plus-square fa-3x fa-inverse" aria-hidden="true"></i></a></li>
					
		</ul>
		</div>
	</div>
</footer>

</body>
</html>