<!DOCTYPE html>
<html>
<head>
	<title>Пополни веб страна</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Encode+Sans+Expanded" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/style2.css">
</head>
<body>

<div class="container">
	<h1 class="text-center"> Еден чекор ве дели до вашата веб страна!</h1>
				 <form class="form" action="contact.php" method="POST">
				 	<label>Напишете го линкот до cover сликата:</label>
				 	<input class="form-control input-lg" type="text" name="cover">

				 	<label>Внесете го насловот:	</label>
				 	<input class="form-control input-lg" type="text" name="title">

				 	<label>Внесете го поднасловот:	</label>
				 	<input class="form-control input-lg" type="text" name="subtitle">
						
				 	<label>Напишете нешто за вас:</label>
				 	 <textarea class="form-control" rows="5" name="foryou" >Внесете повеќе..</textarea>

				 	 <label>Внесете го вашиот телефон:</label>
				 	<input class="form-control input-lg" type="text" name="phone">

				 	<label>Внесете ја вашата локација:</label>
				 	<input class="form-control input-lg" type="text" name="location">

				 	<hr>

					<label>Одберете дали нудите сервиси или продукти:</label>
				 	<select class="form-control input-lg" name="pORs">
						<option value="Сервиси" > Сервиси </option>
						<option value="Продукти"> Продукти </option>
					</select>

					<label>Внесете URL од слика и опис на вашите продукти или сервиси:</label>

				 	<label>URL од слика</label>
				 	<input class="form-control input-lg" type="text" name="url1">
				 	<label>Опис за сликата</label>
				 	 <textarea class="form-control" rows="5" name="desc1"></textarea>

				 	 <label>URL од слика</label>
				 	<input class="form-control input-lg" type="text" name="url2">
				 	<label>Опис за сликата</label>
				 	 <textarea class="form-control" rows="5" name="desc2"></textarea>

				 	 <label>URL од слика</label>
				 	<input class="form-control input-lg" type="text" name="url3">
				 	<label>Опис за сликата</label>
				 	 <textarea class="form-control" rows="5" name="desc3"></textarea>

					<hr>

				 	<label>Напишете нешто за вашата фирма што луѓето треба да го знаат пред да ве контактират:</label>
				 	 <textarea class="form-control" rows="5" name="about"></textarea>

				 	 <hr>

				 	 <label>Linkedin</label>
				 	<input class="form-control input-lg" type="text" name="linkedin" value="https://linkedin.com/"">

				 	<label>Facebook</label>
				 	<input class="form-control input-lg" type="text" name="facebook" value="https://facebook.com/"">

				 	<label>Tweeter</label>
				 	<input class="form-control input-lg" type="text" name="tweeter" value="https://twitter.com/">

				 	<label>Google+</label>
				 	<input class="form-control input-lg" type="text" name="google" value="https://plus.google.com/">

				 	<hr>

					<button type="submit" class="btn-primary center-block text-center proba"> Потврди</button>
</div>

</body>
</html>